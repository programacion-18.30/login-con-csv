document.querySelector("#login_btn").setAttribute("onclick", "validacion()");
let user = document.querySelector("#username");
let pass = document.querySelector("#password");
let mens = document.querySelector("#mensaje");
//---- DOS VARIABLES PARA LA VERIFICACION CON UN SOLO USUARIO-----
//const nombre = "Eugenia";
//const password ="1234";
//-------- ARRAY DE OBJETOS PARA LA VERIFICACION CON VARIOS USUARIOS REGISTRADOS----
fetch("usuarios.csv")
     .then(function (res) { //en res nos regresa el contenido
          
          return (res.text());

     })
     .then(function (data) {
          cargaUsuarios(data);
     });

let usuarios_registrados = [];

function cargaUsuarios(data) {
     let usuarios = data.split(/\r?\n|\r/);
     for (let i = 0; i <  usuarios.length; i++) {
           let fila_usuario = usuarios[i].split(',');
          usuarios_registrados[i] = { nombre: fila_usuario[0], password: fila_usuario[1] };
   
     }
     //console.log(usuarios_registrados);
}

//------RECORRER LOS USUARIOS REGISTRADOS-----
for (let i = 0; i < usuarios_registrados.length; i++) {
     console.log(usuarios_registrados[i]);
}




function validacion() {
     mens.className = "rojo";
     // --------VERIFICACION QUE NO ESTEN LOS INPUT VACIOS----------

     if (user.value.trim() === "") {
          mens.value = "FALTA NOMBRE DE USUARIO";
          user.value = "";
          user.focus();
          return;
     }
     pass.value = pass.value.trim();

     if (pass.value.trim() === "") {
          mens.value = "FALTA PASSWORD";
          pass.value = "";
          pass.focus();
          return;
     }

     //--------VERIFICACION CON UN USUARIO-------------

     // if (nombre !== user.value || password !== pass.value) {
     //    mens.value = "USUARIO INEXISTENTE";
     //  user.focus();
     //user.value = "";
     //pass.value = "";
     //return;
     //}

     //-------VERIFICACION CON VARIOS USUARIOS--------
     let encontroUser;
     for (let i = 0; i < usuarios_registrados.length; i++) {
          // || representan O en una condicion mientras que && representan Y en una condicion
          console.log("estamos recorriendo el elemento", i, ", es decir el usuario  registrado: " + usuarios_registrados[i].nombre);
          if (usuarios_registrados[i].nombre !== user.value || usuarios_registrados[i].password !== pass.value) {
               encontroUser = false;

          } else {
               encontroUser = true;
               console.log("encontrado");
               break;
          }
     }
     if (encontroUser === false) {
          mens.value = "USUARIO INEXISTENTE";
          user.focus();
          user.value = "";
          pass.value = "";
          return;

     }


     mens.className = "verde";
     mens.value = "BIENVENIDA/O " + user.value.toUpperCase();

}